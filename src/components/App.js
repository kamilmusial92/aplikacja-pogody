import React, {Component} from 'react';
import Form from './Form'
import Result from './Result'
import './App.css';

const APIkey='3a6cefec3c1014e63d29035b094570c7';

class App extends Component {

state = {
  value: "",
  date:'',
  city:'',
  sunrise: '',
  sunset:'',
  temp:'',
  pressure:'',
  wind:'',
  err:false,
}

handleInputChange = (e) =>{
  this.setState({
    value: e.target.value
  })
}


componentDidUpdate(prevProps,prevState){

  if(this.state.value.length===0) return
  if(prevState.value !== this.state.value){
    const API = `http://api.openweathermap.org/data/2.5/weather?q=${this.state.value}&APPID=${APIkey}&units=metric`;

  fetch(API)
  .then(response => {
    if(response.ok)
    {
      return response
    }
    throw Error("Nie udało się")
  })
  .then(response => response.json())
  .then(data => {
    const time = new Date().toLocaleString()
    this.setState(state => ({
      err:false,
     
      date: time,
      city: state.value,
      sunrise: data.sys.sunrise,
      sunset: data.sys.sunset,
      temp: data.main.temp,
      pressure: data.main.pressure,
      wind: data.wind.speed,
    }))
  })
  .catch(err => {
    console.log(err);
    this.setState(prevState =>({
      err:  true,
      city: prevState.value
    }))
  
  })
  }
}


render(){


  return (
    <div className="App">
      <Form 
      value={this.state.value} 
      change={this.handleInputChange}
    
      />
      <Result weather={this.state} />
    </div>
    );
  }
}

export default App;
